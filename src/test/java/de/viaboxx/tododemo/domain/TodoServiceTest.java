package de.viaboxx.tododemo.domain;

import de.viaboxx.tododemo.persistence.TodoDao;
import de.viaboxx.tododemo.persistence.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Iterator;

class TodoServiceTest {

    private TodoService todoService;
    private TodoRepository todoRepository;

    @BeforeEach
    void setUp() {
        todoRepository = Mockito.mock(TodoRepository.class);
        todoService = new TodoService(todoRepository);
    }

    @Test
    void allTodos() {
        //given
        Mockito.when(todoRepository.findAll())
                .thenReturn(Arrays.asList(
                        createTodoDao(1L, "test1"),
                        createTodoDao(2L, "test2")
                ));
        //when
        final Iterator<Todo> todos = todoService.allTodos().iterator();
        //then
        assert todos.next().getText().equals("test1");
        assert todos.next().getText().equals("test2");
        assert !todos.hasNext();
    }

    private TodoDao createTodoDao(Long id, String text) {
        final TodoDao todoDao = new TodoDao();
        todoDao.setId(id);
        todoDao.setText(text);
        return todoDao;
    }
}
