package de.viaboxx.tododemo.domain;

public class Todo {

    private final String text;

    public Todo(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
