package de.viaboxx.tododemo.domain;

import de.viaboxx.tododemo.persistence.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    @Autowired
    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public Iterable<Todo> allTodos() {
        return StreamSupport.stream(todoRepository.findAll().spliterator(), false)
                .map(todoDao -> new Todo(todoDao.getText())).collect(Collectors.toList());
    }
}
