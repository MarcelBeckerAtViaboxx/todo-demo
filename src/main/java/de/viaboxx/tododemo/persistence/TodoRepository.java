package de.viaboxx.tododemo.persistence;

import org.springframework.data.repository.CrudRepository;

public interface TodoRepository extends CrudRepository<TodoDao, Long> {
}
