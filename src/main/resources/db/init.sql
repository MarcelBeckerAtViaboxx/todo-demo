create table todo (
  id bigint,
  text text,
  primary key (id)
);

create sequence todo_id_seq;
