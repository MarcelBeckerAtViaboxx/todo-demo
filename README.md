# Getting started

## Setup local DB

Make sure that a PostgreSQL 10 is running on your system, listening on port 5432.

For example, use docker to start a PostgreSQL 10 instance:

    docker run --name todo-tutorial-postgres -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 -d postgres:10

You can connect to the DB with:

    psql -h localhost -p 5432 -U postgres

Enter `mysecretpassword` in the password prompt.

Send the following command to create a user and a database:

    psql -h localhost -p 5432 -U postgres -c "create user todo password 'todo'; create database todo owner todo;"

Send the following command to set up the database schema:

    psql -h localhost -p 5432 -U postgres < src/main/resources/db/init.sql

## Build the Spring Boot application with Gradle

Build the application:

    ./gradlew build

## Start the Spring Boot application with Gradle

Start the application, listening on `localhost:8080`:

    ./gradlew bootRun

## Start the Angular application

Start the Angular application in a development server, listening on `localhost:4200`:

    cd ./frontend-app
    nvm use
    npm run start

