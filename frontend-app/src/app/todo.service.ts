import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Todo } from "./todo";

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: HttpClient) {}

  public retrieveAllTodos(): Observable<Array<Todo>> {
    return new Observable<Array<Todo>>(subscriber => subscriber.next(JSON.parse(localStorage.getItem('todoStorage') || '[]')));
  }

  public storeTodo(todo: Todo): void {
    const todoStorage: Array<Todo> = JSON.parse(localStorage.getItem('todoStorage') || '[]');
    todoStorage.push(todo);
    localStorage.setItem('todoStorage', JSON.stringify(todoStorage));
  }

  public retrieveAllRemoteTodos(): Observable<Array<Todo>> {
    return this.http.get<Array<Todo>>('api/todos');
  }

}
