import { Component, OnInit } from '@angular/core';
import { Todo } from "./todo";
import { TodoService } from "./todo.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  newTodo: Todo = new Todo();
  todos: Array<Todo> = [];
  remoteTodos: Array<Todo> = [];

  constructor(private todoService: TodoService) {}

  ngOnInit(): void {
    this.retrieveAllTodos();
  }

  storeTodo() {
    this.todoService.storeTodo(this.newTodo);
    this.retrieveAllTodos();
  }

  private retrieveAllTodos() {
    this.todoService.retrieveAllTodos().subscribe(todos => this.todos = todos);
    this.todoService.retrieveAllRemoteTodos().subscribe(todos => this.remoteTodos = todos);
  }

}
